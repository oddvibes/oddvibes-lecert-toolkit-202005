# OddVibes LE Certificate Toolkit

## Intro

The OddVibes LE Certificate toolkit is a collection of PHP libraries wrapped in 
easily configurable Symfony commands.

The toolkit has been written for personal use, but since the result may be
very re-usable for other developers, I decided to share it to the public.

The codebase is (and probably will be) a Work In Progress. Any tips, bugs,
or improvement is welcome. And please let me know if you find the toolkit helpful
for you case.

At this moment the toolkit is capable of only two very specific things:

### 1. Request, verify and download LE certificates
The toolkit can request, verify and download Let's Encrypt certificates, 
including wildcard certificates.
The toolkit at this time is capable doing that only by using the TransIP API to 
add ACME records to a domain. This means that the domain where the ACME records
are to be added is registered at TransIP.nl.

It doesn't mean though that the domain you are requesting a certificate for is
registered at TransIP. The domain's ACME record can use a CName to look at the
TransIP domain instead.

### 2. Cleanup ACME records on a domain
The toolkit also has a command to remove "_acme-challenge" records on a domain
registered at TransIP.nl.

## Libraries

1. YAAC (Yet Another ACME Client) - https://github.com/afosto/yaac
2. TransIP API PHP - https://github.com/transip/transip-api-php
3. Several Symfony components - https://symfony.com/components

## Basics

### Requesting a certificate

The first part uses the YAAC PHP library to request a Let's Encrypt certificate.
The script then receives the ACME records that need to be created.

### Domain validation

The script then tries to create the ACME records on the specified domain.
The script uses the TransIP REST API for this.
The domain where the ACME records are created may differ from the domain we 
are requesting the certificate for.

After the records have been created we wait several minutes and ask 
Let's Encrypt to validate the records.

### Download certificate

If validation was succesful we again use the YAAC library to download the
certificates to disk.  
After that you can point the Apache vhost to the certificate.

### Update certificate

The same script can be used to then update the certificate periodically using
the exact same method. Since the download path won't change, the certificate
and the virtual host should keep working.


## Installation

### Prepare the repository

This project exists out of commandline script only, so no virtualhost is needed.  

> git clone https://gitlab.com/oddvibes/oddvibes-lecert-toolkit-202005.git
> cd oddvibes-lecert-toolkit-202005
> composer install --dev

### Settings

Copy `config/parameters.example.yaml` to `parameters.yaml`.  

### app_root

The `%app_root%` parameter is being inherited from the PHP `APP_ROOT` constant, which is being set in bootstrap.php.
The variable can be used to point to directories inside the project.

### certificates.path

The path where the certificates will be downloaded is `%app_root%/data/certificates` by default.
Change this parameter to download them to another location, like `/var/www/certificates`.
Make sure that the directory is writable.

### lets_encrypt.username

Choose a Let's Encrypt username. The account is created automatically on the first call to the LE service.  
A private key will automatically be downloaded to *data/le/[account-name]/account.pem*, which will then be used for all subsequent calls.

### lets_encrypt.mode

The mode is either `staging` or `live`. Please note that the name `staging` in this case can be interpreted as *development mode* or *testing mode*.

The mode is originally implemented to change this setting for the Afosto/Yaac library. 
It will make that all LE calls will be to respectively the *staging* environment or the *live* environment.
The former has no rate-limit, but downloaded certificates will not be accepted by the webbrowser.  

The mode will also change the directory in which the certificates will be downloaded.
For each domain or list of domains for which a certificate is being requested, a sub directory will be created in the `certificates.path`. 
If `lets_encrypt.mode` is set to `staging`, the directory-name will be geprefixed with *DEV-*. 
This will make sure that production certificates won't be overwritten with development certificates. 

### trans_ip.login

The login name of your TransIP account. This is the same login name you use to login to https://www.transip.nl/cp/ .

### trans_ip.private_key_file

In order to use the TransIP API you need an API private key for your account. To get one follow these steps:

1. Navigate to https://www.transip.nl/cp/account/api/  
2. Follow the instruction to create a "Key Pair":
   a. Choose a label. For instance the name of the computer, or the server name. 
   b. You probably want to check the *"Only accept IP-adresses from the whitelist"*.
   c. Save. The private key will be displayed only once, so make sure you copy it immediately.
   d. Create a file, for example *%app_root%/data/transip/[account_name]-[computer_name]-private.key*
      and put the contents of the private key in it.
      Ofcourse the key file can be created somewhere else on the disk. 
   e. Make sure that `trans_ip.private_key_file` points to the private key file.
3. In the TransIP control panel, add one or more IP adresses to the whitelist.
4. You don't have to create Access tokens; These are being generated by the script automatically.
5. When you're done, scroll to the top and enable the API by putting the toggle to *"On"*.

## Commands

The tool is now ready for use. **Always first test it a few times, by putting the `lets_encrypt.mode` to `staging` !!**

### Create and verify a certificate

Simply provide one or more domain names, seperated by a comma.

> bin/app.php lets-encrypt:request-certificate *.localhost.pauldudink.nl,localhost.pauldudink.nl

or

> bin/app.php le:rc *.localhost.pauldudink.nl,localhost.pauldudink.nl

In the above examples the tool will try to:
- Request a single certificate for *.localhost.pauldudink.nl and localhost.pauldudink.nl combined
- It will try to connect to TransIP to create one DNS record: _acme-challenge.localhost.pauldudink.nl
- If succesful, it will download the certificate files to /path/to/certificates/DEV-STAR-localhost.pauldudink.nl_656e8c0ba9358e0ad85ba334b74d265f/

The hash at the end is the MD5-hash of the string "*.localhost.pauldudink.nl,localhost.pauldudink.nl".  
All combined this will function as a fingerprint for the certificate. If the next time a certificate will be requested for *.localhost.pauldudink.nl only, the hash 
will be different and because of that so will the download location.

In case you would like to provide a different domain on which the acme record should be created, then provide the extra parameter containing the full FQDN of
the record, like so:

> bin/app.php le:rc *.localhost.pauldudink.nl,localhost.pauldudink.nl _acme-challenge.example.com

In this case the tool will try to create the ACME record on example.com instead. Let's Encrypt will still perform it's validation requests against 
_acme-challenge.localhost.pauldudink.nl, which then should be a *CNAME* record which points to *_acme-challenge.example.com*.

Please not that multiple ACME-records may exist simultaniously. Let's Encrypt will check all of them to look for the right one.


### Cleanup Acme records

Currently the created acme records are not being removed. The idea is to later implement some code which does remove the record, 
after the certificate has been downloaded succesfully.

Still some records may be left, for example because the script fails. So it is important to cleanup the records now and then.
Be a little careful though, not to remove a record that another client may have just created. In that manner it might be best to pick a time window in which
you can be sure that no other clients are supposed to create new records.

***IMPORTANT NOTICE:** At this time it is impossible to run this script without the confirmation step.
That also means that at this time the cleanup can only be done manually. In the future we may add a --no-confirmation option.  

> bin/app.php dns:cleanup _acme-challenge.localhost.pauldudink.nl

Or

> bin/app.php dns:cl _acme-challenge.localhost.pauldudink.nl

### Code analysis with PHPStan

> vendor/bin/phpstan analyse src/ bin/ --level 3

## Use the downloaded certificates in an Apache vhost

After you have succesfully downloaded certificates with `lets_encrypt.mode` set to `live`,
you can use the certificate in an Apache virtual host.  

Make sure the virtual host listens to port 443 and inside it add the following lines pointing to your certificate files:

    SSLEngine on
    SSLCertificateFile /var/www/certificates/STAR-localhost-pauldudink-nl_3d0f796cdb50ad457122f5841839eadf/certificate.cert
    SSLCertificateKeyFile /var/www/certificates/STAR-localhost-pauldudink-nl_3d0f796cdb50ad457122f5841839eadf/private.key

No seperate intermediate file is required.

After this you can run an Apache config test and reload if succesful.
