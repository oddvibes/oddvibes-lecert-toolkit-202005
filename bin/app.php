#!/usr/bin/env php
<?php

require __DIR__ . '/../bootstrap.php';

use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new \App\Command\RequestCertificateCommand());
$application->add(new \App\Command\CleanUpAcmeRecordsCommand());
$application->run();
