<?php

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use App\Application;

require __DIR__ . '/vendor/autoload.php';

define('APP_ROOT', __DIR__);

$container = new ContainerBuilder();
$loader = new YamlFileLoader($container, new FileLocator(APP_ROOT . '/config'));
$loader->load('services.yaml');

Application::setContainer($container);
