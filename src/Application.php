<?php

namespace App;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Application {

    /**
     * The currently active container object, or NULL if not initialized yet.
     *
     * @var ContainerInterface|null
     */
    protected static $container;

    /**
     * Sets a new global container.
     *
     * @param ContainerInterface $container
     *   A new container instance to replace the current.
     */
    public static function setContainer(ContainerInterface $container) {
        static::$container = $container;
    }

    /**
     * @return ContainerInterface|null
     */
    public static function getContainer() {
        return static::$container;
    }

    public static function getApplicationMode() {
        return static::$container->get('application.mode');
    }

}