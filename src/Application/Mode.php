<?php

namespace App\Application;

use Afosto\Acme\Client;
use App\Exception\Exception;

class Mode {

    /**
     * @var string
     */
    private $mode;

    public function __construct($mode)
    {
        if (!in_array($mode, [Client::MODE_STAGING, Client::MODE_LIVE])) {
            throw new Exception(
                'An application mode has been set that does not match the Acme Client modes we know.' . PHP_EOL
                . 'Either the Afosto Client has been updated / changed, or an invalid mode has been set in the parameters.yaml.' . PHP_EOL
                . 'Please not that if the Afosto Client has been updated / changed, the whole application must be reviewed.' . PHP_EOL
            );
        }

        $this->mode = $mode;
    }

    public function isProductionMode()
    {
        return $this->mode === Client::MODE_LIVE;
    }

}
