<?php

namespace App\Command;

use App\Application;
use App\Dns\AcmeChallengeDomain;
use App\Dns\Api\DnsApiInterface;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CleanUpAcmeRecordsCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'dns:cleanup';

    /**
     * @var array
     */
    private $domains;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @var DnsApiInterface
     */
    private $dnsApi;

    protected function configure()
    {
        $this
            ->setDescription('Cleanup ACME records in the DNS.')
            ->setHelp('This command allows you cleanup remaining ACME records in the DNS once in a while')
            ->setAliases(['dns:cl'])
            ->addArgument('domains', InputArgument::REQUIRED,
                'Provide one or more domains, separated by a comma');
    }
    
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);

        $this->io->writeln(['Welcome to OddVibes DNS Cleaner Toolkit', '']);

        $this->io->writeln(['Initializing clients...', '']);
        $container = Application::getContainer();
        $this->dnsApi   = $container->get('api_proxy');
        $this->dnsApi->setIo($this->io);
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        while (!$input->hasArgument('domains') || !$input->getArgument('domains')) {
            $input->setArgument(
                'domains',
                $this->io->ask('Please provide one or more domains (seperated by commas)')
            );
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Variables
        $this->domains = $this->parseDomainsList($input->getArgument('domains'));

        foreach($this->domains as $domain) {
            $this->io->writeln('');
            $this->dnsApi->deleteAcmeRecords(new AcmeChallengeDomain($domain));
        }

        $this->io->success(['', 'All done.']);

        return 0;
    }

    private function parseDomainsList($domains)
    {
        return explode(',', trim($domains));
    }

}