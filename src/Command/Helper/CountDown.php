<?php

namespace App\Command\Helper;

use App\Exception\Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class CountDown {

    /**
     * @var OutputInterface
     */
    private $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function sleep($seconds, $message = null)
    {
        if (!is_int($seconds)) {
            throw new Exception('Seconds must be an integer');
        }

        if ($message) {
            ProgressBar::setFormatDefinition('custom', ' %current%/%max% -- %message%');
        }

        $progressBar = new ProgressBar($this->output, $seconds);

        if ($message) {
            $progressBar->setFormat('custom');
            $progressBar->setMessage($message);
        }

        $progressBar->start();

        $i = 0;
        while ($i++ < $seconds) {
            sleep(1);
            $progressBar->advance();
        }

        $progressBar->finish();
    }

}