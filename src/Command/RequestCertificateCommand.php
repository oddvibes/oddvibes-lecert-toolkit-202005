<?php

namespace App\Command;

use Afosto\Acme\Data\Authorization;
use App\Application;
use App\Command\Helper\CountDown;
use App\Dns\AcmeChallengeDomain;
use App\Dns\Api\DnsApiInterface;
use App\Dns\Record;
use App\FileSystem\Certificates;

use App\FileSystem\DirectoryName;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use Afosto\Acme\Client;

class RequestCertificateCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'lets-encrypt:request-certificate';

    const dns_propagation_wait_seconds = 120;

    /**
     * @var array
     */
    private $domains;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @var DnsApiInterface
     */
    private $dnsApi;

    /**
     * @var object|null
     */
    private $leClient;

    /**
     * @var Certificates|null
     */
    private $certRepo;

    /**
     * @var CountDown
     */
    private $countDown;

    /**
     * @var DirectoryName
     */
    private $certificateDirectory;

    /**
     * @var Application\Mode
     */
    private $appMode;

    protected function configure()
    {
        $this
            ->setDescription('Request a new certificate.')
            ->setHelp('This command allows you request a new certificate from Lets Encrypt')
            ->setAliases(['le:rc'])
            ->addArgument('domains', InputArgument::REQUIRED,
                'Provide one or more domains, separated by a comma')
            ->addArgument('custom_acme_domain', InputArgument::OPTIONAL,
                'Provide a single FQDN (i.e. _acme-challenge.example.com) where the ACME record point at (when using a CName record). The acme_domain is the one that will be modified.');
    }
    
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->countDown = new CountDown($output);

        $this->io->writeln(['Welcome to the OddVibes LE Certificate Toolkit', '']);

        $this->io->writeln(['Initializing clients...', '']);
        $container = Application::getContainer();
        $this->dnsApi   = $container->get('api_proxy');
        $this->leClient = $container->get('lets_encrypt.client');
        $this->certRepo = $container->get('certificates.repository');
        $this->appMode  = $container->get('application.mode');
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        while (!$input->hasArgument('domains') || !$input->getArgument('domains')) {
            $input->setArgument(
                'domains',
                $this->io->ask('Please provide one or more domains (seperated by commas)')
            );
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Variables
        $this->domains = $this->parseDomainsList($input->getArgument('domains'));

        $this->certificateDirectory = new DirectoryName($this->domains[0] . '_' . md5($input->getArgument('domains')));

        if (!$this->appMode->isProductionMode()) {
            $this->certificateDirectory->addPrefix('DEV-');
            $this->io->success('Running in STAGING mode. Certificate directory: ' . $this->certificateDirectory);

        }else {
            $this->io->warning('Running in LIVE mode. Certificate directory: ' . $this->certificateDirectory);
        }

        // Create order
        $this->io->writeln(sprintf(
            'Creating order for domains [%s]',
            implode(',', $this->domains)
        ));
        $order = $this->leClient->createOrder($this->domains);
        $authorizations = $this->leClient->authorize($order);

        // Authorize
        $this->io->writeln('');
        $this->io->writeln('Create acme DNS records and verify them');
        $this->io->writeln('Please be patient; this may take a while...');
        foreach ($authorizations as $authorization) {
            if ($input->getArgument('custom_acme_domain')) {
                $acmeChallengeDomain = new AcmeChallengeDomain($input->getArgument('custom_acme_domain'));
            }else{
                $acmeChallengeDomain = new AcmeChallengeDomain($authorization->getTxtRecord()->getName());
            }

            $this->createAuthorizationRecord($authorization, $acmeChallengeDomain);
            $this->io->success('DNS record found');
        }

        $this->countDown->sleep(30, 'Wait another 30 seconds for the DNS changes to propagate');

        $this->io->writeln(['', '', 'Asking Let\'s Encrypt to validate the new DNS records']);
        foreach ($authorizations as $authorization) {
            $this->leClient->validate($authorization->getDnsChallenge(), 15);
        }

        if ($this->leClient->isReady($order)) {
            $this->io->success('Record(s) have been validated!');
            $this->io->writeln('Now fetching certificate and saving to disk');
            if ($certificate = $this->leClient->getCertificate($order)) {
                $this->certRepo->writeCertificate($this->certificateDirectory, $certificate);

            }else{
                $this->io->error('Sorry, no certificate received.');
            }
        }

        $this->io->success(['', 'All done. Certificates saved to ' . $this->certificateDirectory]);

        return 0;
    }

    private function parseDomainsList($domains)
    {
        return explode(',', trim($domains));
    }

    private function createAuthorizationRecord(Authorization $authorization, AcmeChallengeDomain $acmeChallengeDomain)
    {
        $txtRecord = new Record((string) $acmeChallengeDomain, $authorization->getTxtRecord()->getValue());

        if (!$this->dnsApi->acmeRecordExists($txtRecord)) {
            $this->io->writeln(sprintf(
                'Creating record %s for domain %s with value %s',
                $txtRecord->getName(), $txtRecord->getDomainName(), $txtRecord->getValue()
            ));

            $this->dnsApi->addAcmeRecord($txtRecord);

            $retry = 10;
            do {
                $this->io->writeln('');
                $this->countDown->sleep(self::dns_propagation_wait_seconds, sprintf('Wait %d seconds for the DNS changes to propagate', self::dns_propagation_wait_seconds));

                $this->io->writeln(['', sprintf('Test if record is available (Retry: %d)', $retry)]);
                if ($this->leClient->selfTest($authorization, Client::VALIDATION_DNS, 1)) {
                    return;
                }
            }while(--$retry);

            throw new \Exception('Could not verify ownership via DNS');

        }else{
            $this->io->success(sprintf(
                'Record %s for domain %s with value %s already exists',
                $txtRecord->getName(), $txtRecord->getDomainName(), $txtRecord->getValue()
            ));
        }
    }
}
