<?php

namespace App\Dns;

use App\Dns\Exception\InvalidDomainException;

class AcmeChallengeDomain {

    /**
     * @var string
     */
    private $acmeChallengeDomain;

    public function __construct($acmeChallengeDomain)
    {
        $this->acmeChallengeDomain = $acmeChallengeDomain;
        $this->test();
    }

    public function getDomain()
    {
        return $this->acmeChallengeDomain;
    }

    public function __toString()
    {
        return $this->getDomain();
    }

    private function test()
    {
        if (false === filter_var($this->acmeChallengeDomain, FILTER_VALIDATE_DOMAIN)) {
            throw new InvalidDomainException('The domain is not a valid domain name');
        }
        if (0 !== strpos($this->acmeChallengeDomain, '_acme-challenge.')) {
            throw new InvalidDomainException('The domain is expected to begin with "_acme-challenge."');
        }
    }

}
