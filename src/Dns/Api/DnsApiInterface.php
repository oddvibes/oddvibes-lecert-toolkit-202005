<?php

namespace App\Dns\Api;

use App\Dns\AcmeChallengeDomain;
use App\Dns\Record;

interface DnsApiInterface {

    /**
     * @param Record $record
     * @return boolean
     */
    public function acmeRecordExists(Record $record);

    /**
     * @param Record $record
     */
    public function addAcmeRecord(Record $record);

    /**
     * @param AcmeChallengeDomain $domain
     */
    public function deleteAcmeRecords(AcmeChallengeDomain $domain);

}
