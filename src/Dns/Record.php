<?php

namespace App\Dns;

class Record {

    /**
     * I.e. pauldudink.nl
     *
     * @var string
     */
    private $domainName;

    /**
     * I.e. _acme-challenge.localhost
     *
     * @var string
     */
    private $recordName;

    /**
     * I.e. _acme-challenge.localhost.pauldudink.nl
     *
     * @var string
     */
    private $fullRecordName;

    /**
     * @var string
     */
    private $value;

    /**
     * Record constructor.
     * @param string $fullRecordName
     * @param string $recordValue
     */
    public function __construct($fullRecordName, $recordValue)
    {
        $this->fullRecordName = $fullRecordName;
        $this->value = $recordValue;
        $this->parse();
    }

    /**
     * @return string
     */
    public function getDomainName()
    {
        return $this->domainName;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->recordName;
    }

    public function getFullName() {
        return $this->fullRecordName;
    }

    /**
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    private function parse()
    {
        $recordParser = new RecordParser($this->fullRecordName);

        $this->recordName = $recordParser->getName();
        $this->domainName = $recordParser->getDomainName();
    }
}
