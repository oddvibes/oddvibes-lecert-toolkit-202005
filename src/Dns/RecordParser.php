<?php

namespace App\Dns;

class RecordParser {

    /**
     * @var string
     */
    private $domainName;

    /**
     * @var string
     */
    private $recordName;

    /**
     * The full record, including domain
     * i.e. *.localhost.pauldudink.nl
     *
     * @var string
     */
    private $fullRecordName;

    /**
     * Record constructor.
     * @param string $fullRecordName
     */
    public function __construct($fullRecordName)
    {
        $this->fullRecordName = $fullRecordName;

        $this->parse();
    }

    /**
     * @return string
     */
    public function getDomainName()
    {
        return $this->domainName;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->recordName;
    }

    public function getFullName() {
        return $this->fullRecordName;
    }

    private function parse()
    {
        preg_match('/(.+)\.(\w+\.\w+)$/i', $this->fullRecordName, $matches);

        $this->recordName = $matches[1];
        $this->domainName = $matches[2];
    }
}
