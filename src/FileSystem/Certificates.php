<?php

namespace App\FileSystem;

use App\FileSystem\Exception\FileException;
use League\Flysystem\Filesystem as FlyFileSystem;
use Afosto\Acme\Data\Certificate;

class Certificates {

    /**
     * @var FlyFileSystem
     */
    private $fileSystem;

    /**
     * Certificates constructor.
     * @param FlyFileSystem $fileSystem
     */
    public function __construct(FlyFileSystem $fileSystem)
    {
        $this->fileSystem = $fileSystem;
    }

    /**
     * @param Certificate $certificate
     */
    public function writeCertificate(DirectoryName $directoryName, Certificate $certificate) {
        $this->createDirectory($directoryName);

        $this->fileSystem->put((string) $directoryName . '/certificate.cert', $certificate->getCertificate());
        $this->fileSystem->put((string) $directoryName . '/private.key', $certificate->getPrivateKey());
    }

    private function createDirectory(DirectoryName $directoryName)
    {
        if ($this->fileSystem->has((string) $directoryName)) {
            if (!$this->fileSystem->get((string) $directoryName)->isDir()) {
                throw new FileException(sprintf(
                    'A file with name "%s" already exists, but it is not a directory',
                    (string) $directoryName
                ));
            }

            return;
        }

        $this->fileSystem->createDir((string) $directoryName);
    }

}
