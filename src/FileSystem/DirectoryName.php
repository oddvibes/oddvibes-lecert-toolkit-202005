<?php

namespace App\FileSystem;

use App\Exception\Exception;

class DirectoryName {

    private const max_length = 80;

    private $cleanName = '';

    public function __construct($dirtyName)
    {
        $this->setDirtyName($dirtyName);
    }

    /**
     * @param string $string
     */
    public function addPrefix(string $string)
    {
        $this->cleanName = $string . $this->cleanName;
    }

    public function getCleanName()
    {
        return $this->cleanName;
    }

    public function __toString()
    {
        return $this->getCleanName();
    }

    private function setDirtyName($dirtyName)
    {
        $wipName = $dirtyName;

        $wipName = trim($wipName);
        $wipName = strtolower($wipName);
        $wipName = str_replace(',', '_', $wipName);
        $wipName = str_replace('*', 'STAR', $wipName);
        $wipName = preg_replace('/[^-\w\d]/', '-', $wipName);
        $wipName = substr($wipName, 0, self::max_length);

        if (!strlen($wipName)) {
            throw new Exception('Directory name must be at least one character long');
        }

        $this->cleanName = $wipName;
    }

}
