<?php

namespace App\TransIp;

use App\Dns\AcmeChallengeDomain;
use App\Dns\Api\DnsApiInterface;
use App\Dns\RecordParser;
use App\Exception\Exception;
use App\TransIp\Exception\PrivateKeyException;
use App\Dns\Api\Exception\ConnectionFailedException;
use App\Dns\Record;

use Symfony\Component\Console\Style\SymfonyStyle;
use Transip\Api\Library\TransipAPI;
use Transip\Api\Library\Entity\Domain\DnsEntry;

class ApiProxy implements DnsApiInterface {

    /**
     * @var TransipAPI
     */
    private $api;

    /**
     * @var SymfonyStyle|null
     */
    private $io;

    /**
     * ApiProxy constructor.
     * @param string $login
     * @param string $privateKeyFilePath
     * @param bool $generateWhitelistOnlyTokens
     * @throws ConnectionFailedException
     * @throws PrivateKeyException
     */
    public function __construct($login, $privateKeyFilePath, $generateWhitelistOnlyTokens)
    {
        if (!file_exists($privateKeyFilePath)) {
            throw new PrivateKeyException('Private key file not found');
        }
        if (!$privateKey = file_get_contents($privateKeyFilePath)) {
            throw new PrivateKeyException('Private key file is empty or unreadable');
        }

        $this->api = new TransipAPI($login, $privateKey, $generateWhitelistOnlyTokens);
        $this->testApiConnection();
    }

    public function setIo(SymfonyStyle $io)
    {
        $this->io = $io;
    }

    /**
     * @throws ConnectionFailedException
     */
    private function testApiConnection()
    {
        // Create a test connection to the api
        if (!$response = $this->api->test()->test()) {
            throw new ConnectionFailedException('TransIp connection test did not return a response');
        }
    }

    /**
     * @param Record $record
     * @return boolean
     */
    public function acmeRecordExists(Record $record) {
        foreach($this->api->domainDns()->getByDomainName($record->getDomainName()) as $existing_record) {
            if ($existing_record->getName() !== $record->getName()) {
                continue;
            }
            if ($existing_record->getContent() === $record->getValue()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Record $record
     */
    public function addAcmeRecord(Record $record) {
        $dnsEntry = new DnsEntry();
        $dnsEntry->setName($record->getName()); // subdomain
        $dnsEntry->setExpire(300);
        $dnsEntry->setType('TXT');
        $dnsEntry->setContent($record->getValue());

        $this->api->domainDns()->addDnsEntryToDomain($record->getDomainName(), $dnsEntry);
    }

    /**
     * @param AcmeChallengeDomain $domain
     */
    public function deleteAcmeRecords(AcmeChallengeDomain $domain) {
        $this->io && $this->io->writeln(sprintf('Cleaning records for "%s"', $domain));

        $recordParser = new RecordParser((string) $domain);

        $recordsToDelete = [];
        foreach($this->api->domainDns()->getByDomainName($recordParser->getDomainName()) as $existingRecord) {
            if ($existingRecord->getName() !== $recordParser->getName()) {
                continue;
            }
            if ($existingRecord->getType() !== 'TXT') {
                $this->io && $this->io->warning('Found matching record, but type is not "TXT", so skipping...');
                continue;
            }

            $this->io && $this->io->writeln(sprintf(
                'Found record "%s" with value "%s"', $existingRecord->getName(), $existingRecord->getContent()
            ));

            $recordsToDelete[] = $existingRecord;
        }

        if (!$recordsToDelete) {
            $this->io && $this->io->warning('No matching records found');
            return;
        }

        $confirmation = $this->io ? $this->io->confirm(sprintf(
            'Do you want to continue and actually delete these %d records?', count($recordsToDelete))) : true;

        if ($confirmation) {
            foreach($recordsToDelete as $existingRecord) {
                $this->api->domainDns()->removeDnsEntry($recordParser->getDomainName(), $existingRecord);
            }

            $this->io && $this->io->success('Records deleted successfully');

        }else{
            $this->io && $this->io->warning('Aborted by user');
        }

    }

}